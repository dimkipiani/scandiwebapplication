<header>
    <div class="wrapper">
        <h3 class="title"><?=$pageTitle ?></h3>
    </div>
</header>

<body>
    <div class="content-wrapper">
        <br>
        <h1>Woops!</h1>
        <h2>Wrong link? Error?</h2>
        <h3>Please visit:</h3>
        <a href="/product/list">The Product List Page</a>
    </div>
</body>