<header>
    <div class="wrapper">
        <h3 class="title"><?=$pageTitle ?></h3>
        <div class="buttons">
            <input type="button" id="submitButton" value="Save" style="margin-right:10px;" />
            <form action="/product/list" method="GET">
                <input type="submit" value="Cancel" style="margin-right:10px;" />
            </form>
        </div>
    </div>
</header>

<div class="content-wrapper">
    <form action="/product/save" id="saveProduct" method="POST">
        <div id="default-form">
            <label for="sku">SKU</label>
            <input type="text" name="product[sku]" id="sku">
            <br>
            <label for="name">Name</label>
            <input type="text" name="product[name]" id="name">
            <br>
            <label for="price">Price ($)</label>
            <input type="text" name="product[price]" id="price">
            <br>
        </div>
        <label for="productType">Choose type</label>
        <select name="productType" id="productType">
            <option value="Book">Book</option>
            <option value="Disc">Disc</option>
            <option value="Furniture">Furniture</option>
        </select>
        <div id="specialForm"></div>
    </form>
</div>


<script>
    $(document).ready(function(){
        updateSpecialForm();
        $("#productType").change(updateSpecialForm);

        $("#submitButton").click(function() {
            
            $(".error-msg").remove();
            
            specialIsFilled = isSpecialFormFilled();
            defaultIsFilled = isDefaultFormFilled();
            if (specialIsFilled && defaultIsFilled) {
                formatSpecialFields();
                formatDefaultFields();
                $("#saveProduct").submit();
            }
        });
    });
</script>