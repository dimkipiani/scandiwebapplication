

<header>
    <div class="wrapper">
        <h3 class="title"><?=$pageTitle ?></h3>
        <div class="buttons">
            <form action="/product/add" method="GET">
                <input type="submit" value="Add" style="margin-right:10px;" />
            </form>
            <form action="/product/delete" id="deleteForm" method="POST">
                <input type="submit" value="Mass Delete" style="margin-right:10px;" />
            </form>

        </div>
    </div>
</header>

<div class="content-wrapper">
    <div class='row'>
        <?php
        foreach($products as $product) {
            require "App/views/helpers/displayProduct.php";
        }
        ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(".product-box").click(function() {
            $(this).toggleClass("selected");
            selected = $(this).find("input").prop('checked');
            $(this).find("input").prop('checked', !selected);
        });
    });
</script>