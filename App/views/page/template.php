<!DOCTYPE html>
<html>

<head>
    <title><?=$pageTitle; ?></title>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="/App/js/app.js"> </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="/App/styles/css/template.css">
    <link rel="stylesheet" href="<?=$style ?>">
</head>

<body>
    <div id="main-wrapper">
        <?php require_once $view ?>

        <footer>
            <div class="wrapper">
                <h4>Scandiweb Test assignment</h4>
            </div>
        </footer>
    </div>
</body>

</html>