<?php
    $sku = htmlspecialchars($product->getSku());
    $attributeList = "";
    foreach($product->getAttributesAsString() as $attribute) {
        $attributeList .= "<span>{$attribute}</span> <br>";
    }
?>

<div class='box col-md-3 col-sm-3 demo-container'>
    <div class='product-box'>
        <input id="<?=$sku; ?>" type='checkbox' form='deleteForm' 
            name='checkmark[]' class='checkmark' value="<?=$sku; ?>">
        <?=$attributeList; ?>
    </div>
</div>