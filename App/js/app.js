emptyError = "<span class='error-msg'>* Please, submit required data</span>";
formatError = "<span class='error-msg'>* Please, provide the data of indicated type</span>"


function isDefaultFormFilled() {
    isFilled = !isFieldEmpty('sku');
    isFilled &= !isFieldEmpty('name');
    isFilled &= isFieldFloat('price');
    return isFilled;
}

function isFieldEmpty(field) {
    fieldVal = $('#' + field).val();
    if (fieldVal === "") {
        $('#' + field).after(emptyError);
        return true;
    }
    return false;
}

function isFieldInt(field) {
    if (isFieldEmpty(field)) return false;
    parsed = Number($('#' + field).val());
    if (!Number.isInteger(parsed) || parsed <= 0) {
        $('#' + field).after(formatError);
        return false;
    }
    return true;
}

function isFieldFloat(field) {
    if (isFieldEmpty(field)) return false;
    parsed = Number($('#' + field).val());
    if (isNaN(parsed) || parsed <= 0) {
        $('#' + field).after(formatError);
        return false;
    }
    return true;
}

function formatDefaultFields() {
    formatted = $('#sku').val().trim();
    $('#sku').val(formatted);
    formatted = $('#name').val().trim();
    $('#name').val(formatted);
}

function formatSpecialFields() {
    // None needed
}

function updateSpecialForm(){
    $("#specialForm").empty();
    type = $("#productType").val().toLowerCase();
    formUrl = "/App/views/forms/" + type + "-form.html";
    $("#specialForm").load(formUrl);
}

