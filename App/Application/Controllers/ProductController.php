<?php
namespace Application\Controllers;

use Application\Models\Database\DAL;
use Application\Models\Product\{Disc, Furniture, Book};
use Application\Models\View;

class ProductController
{
    public function __construct()
    {
    }

    public function listAction()
    {
        $dal = new DAL();
    
        $products = array();
        foreach($dal->getDiscs() as $entity) {
            array_push($products, Disc::fromArray($entity));
        }
    
        foreach($dal->getFurnitures() as $entity) {
            array_push($products, Furniture::fromArray($entity));
        }
    
        foreach($dal->getBooks() as $entity) {
            array_push($products, Book::fromArray($entity));
        }

        $pageTitle = "Product List";
        View::render("listProduct", compact('pageTitle', 'products'));
    }

    public function addAction()
    {
        $pageTitle = "Product Add";
        View::render("addProduct", compact('pageTitle'));
    }

    public function saveAction()
    {
        $type = $_POST['productType'];
        $class = 'Application\Models\Product\\'.$type;
        if (class_exists($class)) {
            $obj = $class::fromArray($_POST['product']);
            $dal = new DAL();
            $saveMethod = 'save'.$type;
            $dal->$saveMethod($obj);
            $this->redirect("/product/list");
        }
    }

    public function deleteAction()
    {
        $prods = $_POST['checkmark'];
        $dal = new DAL();
        foreach($prods as $productSku) {
            $dal->deleteProduct($productSku);
        }
        $this->redirect('/product/list');
    }

    private function redirect(string $url)
    {
        ob_start();

        while (ob_get_status()) 
        {
            ob_end_clean();
        }

        header("Location: $url");
    }
}
