<?php
namespace Application\Controllers;

use Application\Models\View;

class HomeController
{
    public function __construct()
    {
    }

    public function indexAction()
    {
        $pageTitle = "Home";
        View::render("indexHome", compact('pageTitle'));
    }
}
