<?php
namespace Application\Models\Product;

trait ProductTrait
{
    public $sku = "";
    public $name = "";
    public $price = 0.0;

    public static function fromArray(array $data)
    {
        $self = new self();
        $self->sku = $data['sku'];
        $self->name = $data['name'];
        $self->price = $data['price'];
        return $self;
    }
}
