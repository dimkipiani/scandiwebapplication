<?php
namespace Application\Models\Product;

use Application\Models\Product\{Product, ProductTrait};
 
class Furniture extends Product
{
    use ProductTrait {
        fromArray as parentFromArray;
    }

    private $height;
    private $width;
    private $length;

    public function __construct()
    {
    }

    public static function fromArray(array $data)
    {
        $self = self::parentFromArray($data);
        $self->height = $data['height'];
        $self->width = $data['width'];
        $self->length = $data['length'];
        return $self;
    }

    public function getDimensions()
    {
        return array($this->height, $this->width, $this->length);
    }

    public function getAttributesAsString()
    {
        $attributes = parent::getAttributesAsString();
        array_push(
            $attributes, 
            "Dimension: {$this->height}x{$this->width}x{$this->length}"
        );
        return $attributes;
    }
}
