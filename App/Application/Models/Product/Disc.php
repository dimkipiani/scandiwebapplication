<?php
namespace Application\Models\Product;

use Application\Models\Product\{Product, ProductTrait};
 
class Disc extends Product
{
    use ProductTrait {
        fromArray as parentFromArray;
    }

    private $size;

    public function __construct()
    {
    }

    public static function fromArray(array $data)
    {
        $self = self::parentFromArray($data);
        $self->size = $data['size'];
        return $self;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getAttributesAsString()
    {
        $attributes = parent::getAttributesAsString();
        array_push($attributes, "Size: {$this->size} MB");
        return $attributes;
    }
}
