<?php
namespace Application\Models\Product;

use Application\Models\Product\ProductTrait;

class Product
{
    use ProductTrait;
    
    private function __construct()
    {
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getAttributesAsString()
    {
        return array($this->sku, $this->name, "$this->price $");
    }

    public function getSku()
    {
        return $this->sku;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }
}
