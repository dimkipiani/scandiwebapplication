<?php
namespace Application\Models\Product;

use Application\Models\Product\{Product, ProductTrait};

class Book extends Product
{
    use ProductTrait {
        fromArray as parentFromArray;
    }
    
    private $weight = 0;

    private function __construct()
    {
    }

    public static function fromArray(array $data)
    {
        $self = self::parentFromArray($data);
        $self->weight = $data['weight'];
        return $self;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight(int $weight)
    {
        $this->weight = $weight;
    }

    public function getAttributesAsString()
    {
        $attributes = parent::getAttributesAsString();
        array_push($attributes, "Weight: {$this->weight}KG");
        return $attributes;
    }
}
