<?php
namespace Application\Models;

use Config\Application;

class View
{
    public static function render(string $page, array $params)
    {
        extract($params, EXTR_SKIP);
        $view = "App/views/page/$page.php";
        $style = Application::BASE_DIR."App/styles/css/$page.css";

        require_once "App/views/page/template.php";
    }
}
