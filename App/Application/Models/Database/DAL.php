<?php
namespace Application\Models\Database;

use \mysqli;
use Application\Models\Product\{Disc, Furniture, Book};
use Config\Database;

class DAL
{
    private $mysqli = null;

    public function __construct()
    {
    }

    private function dbConnect()
    {
        $this->mysqli = new mysqli(
            Database::HOST,
            Database::USER,
            Database::PASSWORD,
            Database::NAME
            ) or die ("<br/>Could not connect to MySQL server");
    }

    private function dbClose()
    {
        $this->mysqli->close();
    }

    private function query($sql)
    {
        $res = $this->mysqli->query($sql);

        if ($res) {
            if (strpos($sql, 'SELECT') === false) {
                return true;
            }
        }
        else {
            if (strpos($sql, 'SELECT') === false) {
                return false;
            }
            else {
                return null;
            }
        }

        $results = array();

        while ($row = $res->fetch_assoc()) {
            $results[] = $row;
        }

        return $results;
    }

    public function getFurnitures()
    {
        $this->dbconnect();

        $sql = "SELECT *
                FROM products p
                JOIN furnitures f
                ON f.sku = p.sku";
        $result = $this->query($sql);
        $this->dbClose();
        return $result;
    }

    public function getDiscs()
    {
        $this->dbconnect();

        $sql = "SELECT *
                FROM products p
                JOIN discs d
                ON d.sku = p.sku";
        $result = $this->query($sql);
        $this->dbClose();
        return $result;
    }

    public function getBooks()
    {
        $this->dbconnect();

        $sql = "SELECT *
                FROM products p
                JOIN books b
                ON b.sku = p.sku";
        $result = $this->query($sql);
        $this->dbClose();
        return $result;
    }

    public function deleteProduct(string $sku)
    {
        $this->dbconnect();

        $sku = $this->escapeString($sku);
        $sql = "DELETE
                FROM products
                WHERE products.sku = ('$sku')";
        $result = $this->query($sql);

        $this->dbClose();
        return $result;
    }

    public function saveFurniture(Furniture $furniture)
    {
        $this->dbconnect();
        $result = $this->saveProduct($furniture);

        if ($result) {
            $sku = $this->escapeString($furniture->getSku());
            $dimensions = $furniture->getDimensions();
            $height = $this->escapeString($dimensions[0]);
            $width = $this->escapeString($dimensions[1]);
            $length = $this->escapeString($dimensions[2]);
            $sql = "INSERT
                    INTO `furnitures` (`sku`, `height`, `width`, `length`)
                    VALUES ('$sku', '$height', '$width', '$length')";
            $result = $this->query($sql);
        }

        $this->dbClose();
        return $result;
    }

    public function saveDisc(Disc $disc)
    {
        $this->dbconnect();
        $result = $this->saveProduct($disc);

        if ($result) {
            $sku = $this->escapeString($disc->getSku());
            $size = $this->escapeString($disc->getSize());
            $sql = "INSERT
                    INTO `discs` (`sku`, `size`)
                    VALUES ('$sku', '$size')";
            $result = $this->query($sql);
        }

        $this->dbClose();
        return $result;
    }

    public function saveBook(Book $book)
    {
        $this->dbconnect();
        $result = $this->saveProduct($book);

        if ($result) {
            $sku = $this->escapeString($book->getSku());
            $weight = $this->escapeString($book->getWeight());
            $sql = "INSERT
                    INTO `books` (`sku`, `weight`)
                    VALUES ('$sku', '$weight')";
            $result = $this->query($sql);
        }

        $this->dbClose();
        return $result;
    }

    // It is assumed that connection is already made
    private function saveProduct($product)
    {
        $sku = $this->escapeString($product->getSku());
        $name = $this->escapeString($product->getName());
        $price = $this->escapeString($product->getPrice());
        $sql = "INSERT
                INTO `products` (`sku`, `name`, `price`)
                VALUES ('$sku', '$name', '$price')";
        return $this->query($sql);
    }

    private function escapeString($var)
    {
        return $this->mysqli->real_escape_string($var);
    }
}
