<?php
namespace Config;

class Database
{
    const HOST = 'localhost';
    const USER = 'root';
    const PASSWORD = 'mysql';
    const NAME = 'db_scandiweb_test';
}
