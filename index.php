<?php

require __DIR__.'/vendor/autoload.php';
use Config\Application;
use Application\Models\View;

$requestUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$requestString = substr($requestUrl, strlen(Application::BASE_URL));
if ($requestString === '') {
    $requestString = 'home/index';
}

$urlParams = explode('/', strtok($requestString, '?'));

$controllerName = ucfirst(array_shift($urlParams));
$controllerClass = '\Application\Controllers\\'.$controllerName.'Controller';

if (class_exists($controllerClass)) {
    $actionName = strtolower(array_shift($urlParams)).'Action';
    $controller = new $controllerClass();
    if (method_exists($controller, $actionName)) {
        $controller->$actionName();
    } else {
        $pageTitle = 'Not Found';
        View::render('errorPage', compact('pageTitle'));
    }
} else {
    $pageTitle = 'Not Found';
    View::render('errorPage', compact('pageTitle'));
}
