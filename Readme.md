# Test Assignment for Scandiweb

This is the entire source code for the assignment.

## Website
- [Product List](https://scandiweb-test-w123ewq.000webhostapp.com/product/list)
- [Product Add](https://scandiweb-test-w123ewq.000webhostapp.com/product/add)
- [Index](https://scandiweb-test-w123ewq.000webhostapp.com/)

## Database
```MySQL
CREATE TABLE products (
    sku VARCHAR(10) PRIMARY KEY,
    name VARCHAR(25) NOT NULL,
    price FLOAT NOT NULL
);

CREATE TABLE discs (
    sku VARCHAR(10) PRIMARY KEY,
    size INT NOT NULL,
	FOREIGN KEY (sku)
		REFERENCES products (sku)
		ON DELETE CASCADE
);

CREATE TABLE books (
    sku VARCHAR(10) PRIMARY KEY,
    weight INT NOT NULL,
	FOREIGN KEY (sku)
		REFERENCES products (sku)
		ON DELETE CASCADE
);

CREATE TABLE furnitures (
    sku VARCHAR(10) PRIMARY KEY,
    height INT NOT NULL,
    width INT NOT NULL,
    length INT NOT NULL,
	FOREIGN KEY (sku)
		REFERENCES products (sku)
		ON DELETE CASCADE
);
```